#!/bin/bash

function load_movie 
{
        MOVIE_PATH="$1"
        THUMBNAIL_DIR=$(dirname "$MOVIE_PATH")/thumbnails
        mkdir -p "$THUMBNAIL_DIR"
        IMG_NAME=$(basename "${MOVIE_PATH%.mp4}.jpg")
        IMG_PATH="$THUMBNAIL_DIR/$IMG_NAME"
        IMG_PATH=`realpath $IMG_PATH`
        gst-launch-1.0 -q filesrc location=$IMG_PATH ! jpegdec  ! videoscale ! videoconvert ! fbdevsink &
        killall vlc
        cvlc --quiet --no-embedded-video --vout drm_vout --no-osd --input-repeat 9999999999999999 $MOVIE_PATH &
}

load_movie $1

echo "load movie path : $1"
