#!/bin/bash

# Input directory where movie files are located
input_dir="/home/pi/media/dm"

# Output directory where thumbnails will be saved
output_dir="$input_dir/thumbnails"

# Create the output directory if it doesn't already exist
mkdir -p "$output_dir"
echo $output_dir
# Loop through each movie file in the input directory
for file in "$input_dir"/*
do
  # Check if file is a movie file
  echo $file
  if [[ "${file##*.}" =~ ^(mp4|avi|mkv|mov)$ ]]
  then
    # Extract the first frame of the movie file using ffmpeg
    echo "---> $file"
    ffmpeg -i  "$file" -vf "scale=w=1280:h=800:force_original_aspect_ratio=decrease,pad=1280:800:(ow-iw)/2:(oh-ih)/2"  -vframes 1 "$output_dir/$(basename "${file%.*}").jpg"
  fi
done
